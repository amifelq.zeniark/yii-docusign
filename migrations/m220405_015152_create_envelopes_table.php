<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%envelopes}}`.
 */
class m220405_015152_create_envelopes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%envelopes}}', [
            'id' => 'INT PRIMARY KEY AUTO_INCREMENT',
            'envelope_id' => 'VARCHAR(255) NOT NULL',
            'template_id' => 'VARCHAR(255) NOT NULL',
            'status' => 'VARCHAR(255) NOT NULL',
            'date_envelope_sent' => 'DATETIME NULL DEFAULT NOW()',
            'roles' => 'TEXT NOT NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%envelopes}}');
    }
}
