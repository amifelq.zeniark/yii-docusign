<?php

namespace app\components;

use Yii;

class Config {
  public static function get($name = '') {
    if (!$name || !isset(Yii::$app->params[$name])) {
      return null;
    }

    return Yii::$app->params[$name];
  }
}