<?php

namespace app\components;

use Yii;
use yii\web\Response as BaseResponse;

class Response {
  public static function send($data, $statusCode = 200) {
    $response = Yii::$app->response;
    $response->format = BaseResponse::FORMAT_JSON;
    $response->statusCode = $statusCode;
    $response->data = $data;
  }
}