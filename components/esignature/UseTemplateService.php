<?php

namespace app\components\esignature;

use DocuSign\eSign\Model\EnvelopeDefinition;
use DocuSign\eSign\Model\TemplateRole;

class UseTemplateService
{
    /**
     * Do the work of the example
     * 1. Create the envelope request object
     * 2. Send the envelope
     *
     * @param  $args array
     * @param $clientService
     * @return array ['redirect_url']
     */
    # ***DS.snippet.0.start
    public static function useTemplate(array $args, $clientService): array
    {
        # 1. Create the envelope request object
        $envelope_definition = UseTemplateService::make_envelope($args["envelope_args"]);

        # 2. call Envelopes::create API method
        # Exceptions will be caught by the calling function
        $envelope_api = $clientService->getEnvelopeApi();
        $envelopeResponse = $envelope_api->createEnvelope($args['account_id'], $envelope_definition);

        return ['envelope_id' => $envelopeResponse->getEnvelopeId()];
    }

    /**
     * Creates envelope definition using a template
     * Parameters for the envelope: signer_email, signer_name, signer_client_id
     *
     * @param  $args array
     * @return mixed -- returns an envelope definition
     */
    public static function make_envelope(array $args): EnvelopeDefinition
    {
        # create the envelope definition with the template_id
        $envelope_definition = new EnvelopeDefinition([
           'status' => 'sent', 'template_id' => $args['template_id']
        ]);
        # Create the template role elements to connect the signer and cc recipients
        # to the template
        $index = 0;
        $signers = array();
        foreach($args['signers'] as $signer) {
            $signer = new TemplateRole([
                'email' => $args['signers'][$index]['signer_email'],
                'name' => $args['signers'][$index]['signer_name'],
                'role_name' => 'signer'
            ]);
            array_push($signers, $signer);
            $index++;
        }
        // $signer = new TemplateRole([
        //     'email' => $args['signer_email'], 'name' => $args['signer_name'],
        //     'role_name' => 'signer'
        // ]);
        # Create a cc template role.
        $index1 = 0;
        $ccs = array();
        foreach($args['ccs'] as $cc) {
            $cc = new TemplateRole([
                'email' => $args['ccs'][$index1]['cc_email'],
                'name' => $args['ccs'][$index1]['cc_name'],
                'role_name' => 'cc'
            ]);
            array_push($ccs, $cc);
            $index1++;
        }
        // $cc = new TemplateRole([
        //     'email' => $args['cc_email'], 'name' => $args['cc_name'],
        //     'role_name' => 'cc'
        // ]);
        # Add the TemplateRole objects to the envelope object
        // $envelope_definition->setTemplateRoles([$signer, $cc]);
        $envelope_definition->setTemplateRoles(array_merge($signers, $ccs));
        return $envelope_definition;
    }
    # ***DS.snippet.0.end
}
