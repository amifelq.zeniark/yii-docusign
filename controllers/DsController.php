<?php

namespace app\controllers;

use app\components\DocuSign;
use app\models\Token;
use Yii;
use yii\web\Controller;
use app\components\Response;
use app\components\Config;
use app\components\SignatureClientService;
use app\components\esignature\UseTemplateService;
use Exception;
use yii\web\Request;

class DsController extends Controller
{
  // protected SignatureClientService $clientService;
  protected array $args;

  // public function __construct()
  // {
  //   $this->clientService = new SignatureClientService($this->args);
  // }

  public function actionIndex()
  {
    echo "Test";
  }

  public function actionLogin()
  {
    Response::send([
      'message' => 'Click URL to consent Docusign',
      'url' => Config::get('authorization_server') . '/oauth/auth?response_type=code&scope=signature&client_id=be5ea2ae-401f-44bc-ab30-459c23e9af24&redirect_uri=http://localhost:8080/ds/callback',
    ]);
  }

  public function actionCallback()
  {
    // try {
      $request = Yii::$app->request;
      $provider = new DocuSign(
        [
          'clientId' => Config::get('ds_client_id'),
          'clientSecret' => Config::get('ds_client_secret'),
          'redirectUri' => Config::get('app_url')  . 'ds/callback',
          'authorizationServer' => Config::get('authorization_server'),
          'allowSilentAuth' => Config::get('allow_silent_authentication'),
        ]
      );

      $accessToken = $provider->getAccessToken(
        'authorization_code',
        [
          'code' => $_GET['code']
        ]
      );
      $user = $provider->getResourceOwner($accessToken);
      $account_info = $user->getAccountInfo();
      $base_uri_suffix = '/restapi';

      var_dump($account_info);
      var_dump($user->getEmail());
      
      $token = new Token;
      $token->auth_code = $_GET['code'];
      $token->access_token = $accessToken->getToken();
      $token->refresh_token = $accessToken->getRefreshToken();
      $token->exp_time = $accessToken->getExpires();
      $token->user_name = $user->getName();
      $token->user_email = $user->getEmail();
      $token->account_id = $account_info["account_id"];
      $token->account_name = $account_info["account_name"];
      $token->basepath = $account_info["base_uri"] . $base_uri_suffix;
      $token->save(false);

      // $session = Yii::$app->session;
      // if (!$session->isActive) $session->open();

      // $session->set('ds_access_token', $accessToken->getToken());
      // var_dump($session);
      $cookies = Yii::$app->response->cookies;
      $cookies->add(new \yii\web\Cookie([
        'name' => 'ds_access_token',
        'value' => $accessToken->getToken(),
      ]));
      
      Response::send([
        'message' => 'Successfully consented app!',
        'data' => [
          'auth_code' => $token->auth_code,
          'access_token' => $token->access_token,
          'refresh_token' => $token->refresh_token,
          'exp_time' => $token->exp_time,
        ]
      ]);

    // } catch (Exception $e) {
    //   Response::send([
    //     'message' => $e->getMessage(),
    //   ], 500);
    // }
  }

  public function actionEnvelope()
  {
    // try {
      // if (!Yii::$app->request->isPost) {
      //   Response::send([
      //     'message' => 'URL not found',
      //   ], 404);
      // }
      $cookies = Yii::$app->request->cookies;

      $signers = array();
      array_push($signers, [
        'signer_email' => 'amifel.quejado@gmail.com',
        'signer_name' => 'Amifel Qaxandra Quejado',
      ]);

      $ccs = array();
      array_push($ccs, [
        'cc_email' => 'amifelq.zeniark@gmail.com',
        'cc_name' => 'Carrier',
      ]);

      $envelope_args = [
        'signers' => $signers,
        'ccs' => $ccs,
        'template_id' => 'd1a76d21-cf3d-4fb9-bee9-5d49d5837f9e',
      ];

      // $access_token = $cookies->get('ds_access_token');
      $access_token = 'eyJ0eXAiOiJNVCIsImFsZyI6IlJTMjU2Iiwia2lkIjoiNjgxODVmZjEtNGU1MS00Y2U5LWFmMWMtNjg5ODEyMjAzMzE3In0.AQoAAAABAAUABwAALPtnzBbaSAgAAGwedg8X2kgCADyXHitITIxKnaaIDCCRQLEVAAEAAAAYAAEAAAAFAAAADQAkAAAAYmU1ZWEyYWUtNDAxZi00NGJjLWFiMzAtNDU5YzIzZTlhZjI0IgAkAAAAYmU1ZWEyYWUtNDAxZi00NGJjLWFiMzAtNDU5YzIzZTlhZjI0MAAAAM7enxbaSDcAzcuOf_eoaUCqJwd0Id5qlQ.YtTkhaY3EIh7cgWVr5VadcEulR30zKJ7ck-WSkSYqNbl7dsgeLayHRr9ADrZMxVLRrkD5pvF8tyZ4cJN-sSBsTkQcEenV8H49L2ANirs8CXeoqdsisaMCDD4UqY7wcBVzH9h28nclwMB0a_f_j-4hdw-4xcwx73hePKp1MSCniS-WbmPTWj7-rvK2B-B_ax-GGaro3lj_-5Yx85aimgMziH34RIhZGyoinkyrYgXNB_oV4eca7s5AVYzV7L83JXFkfsKOIZ5QLUsuYCXbUXc1xO_XLnI2_lDEjIyhZLc3xKMZuh0uNYkUO0VQXM-ssoPIFElRm-teuMUqIMoiQx04w';

      $token = Token::find()
        ->where([
          'access_token' => $access_token,
        ])
        ->one();
          
      $args = [
        'account_id' => $token->account_id,
        'base_path' => $token->basepath,
        'ds_access_token' => $access_token,
        'envelope_args' => $envelope_args
      ];
  
      $clientService = new SignatureClientService($args);

      if ($clientService) {
        $envelopeId = UseTemplateService::useTemplate($args, $clientService);
  
        if ($envelopeId) {
        // TODO: save envelope ID
    
          return Response::send([
            'message' => 'Envelope successfully sent!',
            'data' => [
              'envelope_id' => $envelopeId["envelope_id"]
            ]
          ]);
        }
      }
  
      Response::send([
        'message' => 'Invalid request',
      ], 400);
    // } catch (Exception $e) {
    //   Response::send([
    //     'message' => $e->getMessage(),
    //   ], 500);
    // }
    
  }

  public function actionTest() {
    Response::send([
      'message' => 'Success',
    ]);
  }
}
