<?php

return [
  'adminEmail' => 'admin@example.com',
  'senderEmail' => 'noreply@example.com',
  'senderName' => 'Example.com mailer',
  'ds_client_id' => 'be5ea2ae-401f-44bc-ab30-459c23e9af24',  // The app's DocuSign integration key
  'ds_client_secret' => '5b68bba4-66d3-428a-8683-2f7160b07279', // The app's DocuSign integration key's secret
  'authorization_server' => 'https://account-d.docusign.com',
  'app_url' => 'http://localhost:85',
  'allow_silent_authentication' => true,
  'target_account_id' => false, // Set if you want a specific DocuSign AccountId, If false, the user's default account will be used.
];
